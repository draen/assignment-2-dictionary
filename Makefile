ASM=nasm
ASM_FLAGS=-g -felf64
LD=ld

build: clean out/main
run: build
	./out/main
out/main: build/main.o build/dict.o build/lib.o
	$(LD) -o $@ $^

build/%.o: src/%.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

clean:
	rm -rf build/*
	rm -rf out/*

.PHONY: build clean run