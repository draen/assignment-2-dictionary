%include "src/lib.inc"
%define POINTER_SIZE 8

section .text

global find_word
find_word:
    ; rdi: key string pointer
    ; rsi: linked list pointer


.loop:
    add rsi, POINTER_SIZE   ;move colon pointer to the colon key
    push rdi
    push rsi
    call string_equals      ;check if key is found
    pop rsi
    pop rdi
    sub rsi, POINTER_SIZE   ;return pointer to the colon beginning

    test rax,rax
    jnz .found              ;keys match (from string_equals)

    mov rsi, [rsi]          ;move next element address to rsi
    test rsi, rsi           ;if pointer to next element is null - key not found
    jnz .loop
.not_found:
    xor rax,rax
    ret
.found:
    mov rax, rsi
    ret