%include "src/colon.inc"
%include "src/words.inc"
%include "src/lib.inc"

%define BUFFER_SIZE 256
%define POINTER_SIZE 8

extern find_word

section .rodata
    hello_message: db "Welcome to Awesome Dictionary 101(tm), what are you looking for?", 0
    error_overflow_message: db "Sorry, your search query is too long", 0
    error_not_found_message: db "No info found... Impossible, perhaps the archives are incomplete", 0
    key_found_message: db "Found your query!", 0
    key_value_separator: db ": ", 10, 0

section .bss
    buffer: resb BUFFER_SIZE

section .text
global _start
_start:
.hello:
    mov rdi, hello_message      ;self-explanatory - just print greeting
    call print_string
    call print_newline
.read:
    mov rdi, buffer             ;also simple - just read a word
    mov rsi, 256
    call read_word

    push rdx                    ;save key length
    
    test rax,rax                ;if buffer overflow - error
    jz .error_overflow

    mov rdi, rax                ;try to find word in dictionary
    mov rsi, NEXT_ELEMENT
    call find_word
    test rax,rax                ;if not found - error
    jz .error_not_found

.print:
    push rax
    mov rdi, key_found_message
    call print_string
    call print_newline
    pop rax

    add rax, POINTER_SIZE       ;move pointer to key
    mov rdi, rax

    push rdi
    call print_string
    mov rdi, key_value_separator;print separator between key & value
    call print_string
    pop rdi

    pop rdx
    add rdi, rdx                ;move pointer over key to value
    inc rdi
    call print_string
    call print_newline
    xor rdi,rdi
    call exit


.error_not_found:
    mov rdi, error_not_found_message
    jmp .error_exit
.error_overflow:
    mov rdi, error_overflow_message
.error_exit:
    call print_error
    call print_newline
    mov rdi, 1
    call exit